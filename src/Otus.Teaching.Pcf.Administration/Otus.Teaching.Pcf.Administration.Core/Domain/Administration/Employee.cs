﻿using System;
using System.Collections.Generic;
using MongoDB.Bson.Serialization.Attributes;

namespace Otus.Teaching.Pcf.Administration.Core.Domain.Administration
{
    public class Employee
        : BaseEntity
    {
        [BsonElement("first_name")]
        public string FirstName { get; set; }
        [BsonElement("last_name")]
        public string LastName { get; set; }
        [BsonElement("fullname")]
        public string FullName { get; set; }
        [BsonElement("email")]
        public string Email { get; set; }
        [BsonElement("role_id")]
        public string RoleId { get; set; }
        public virtual Role Role { get; set; }
        [BsonElement("promo_codes_count")]
        public int AppliedPromocodesCount { get; set; }
    }
}
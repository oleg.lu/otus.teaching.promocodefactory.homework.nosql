﻿using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using Otus.Teaching.Pcf.Administration.DataAccess.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Repositories
{
    public class EmployeeService : IEmployeeService
    {
        private readonly IMongoCollection<Employee> _employeeRepository;
        //public EmployeeService(IAdminStoreDatabaseSettings settings, IMongoClient mongoClient)
        //{
        //    var database = mongoClient.GetDatabase(settings.DatabaseName);
        //    _employeeRepository = database.GetCollection<Employee>(settings.AdminRolesCollectionName);
        //}

        public EmployeeService(IMongoDatabase database)
        {
            _employeeRepository = database.GetCollection<Employee>("Employees");
        }

        public Employee Create(Employee employee)
        {
            _employeeRepository.InsertOne(employee);
            return employee;
        }

        public List<Employee> Get()
        {
            return _employeeRepository.Find(employee => true).ToList();
        }

        public Employee Get(string id)
        {
            return _employeeRepository.Find(employee => employee.Id == id).FirstOrDefault();
        }

        public void Remove(string id)
        {
            _employeeRepository.DeleteOne(employee => employee.Id == id);
        }

        public void Update(string id, Employee employee)
        {
            _employeeRepository.ReplaceOne(employee => employee.Id == id, employee);
        }
    }
}

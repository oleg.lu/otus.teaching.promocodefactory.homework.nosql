﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Repositories
{
    public class MongoRepository<T>
        : IRepository<T>
        where T : BaseEntity
    {
        private readonly IMongoCollection<T> _dataContext;

        public MongoRepository(IMongoDatabase database)
        {
            _dataContext = database.GetCollection<T>($"{nameof(T)}s");
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            var entities = await (await _dataContext.FindAsync(_ => true)).ToListAsync();
            return entities;
        }

        public async Task<T> GetByIdAsync(string id)
        {
            var entity = (await _dataContext.FindAsync(entity => entity.Id == id)).FirstOrDefault();

            return entity;
        }



        public async Task AddAsync(T entity)
        {
            await _dataContext.InsertOneAsync(entity);
        }


        public async Task UpdateAsync(string id, T entity)
        {
            await _dataContext.ReplaceOneAsync(entity => entity.Id == id, entity);
        }

        public async Task DeleteAsync(string id)
        {
            _dataContext.DeleteOne(entity => entity.Id == id);

        }
    }
}
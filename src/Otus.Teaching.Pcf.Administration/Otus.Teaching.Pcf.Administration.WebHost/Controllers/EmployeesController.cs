﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeesRepository;


        public EmployeesController(IRepository<Employee> employeesRepository)
        {
            _employeesRepository = employeesRepository;
        }

        /// <summary>
        /// Создать сотрудника
        /// </summary>
        /// <param name="employee"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task Post([FromBody] Employee employee)
        {
            await _employeesRepository.AddAsync(employee);
        }

        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IEnumerable<Employee>> GetEmployees()
        {
            return await _employeesRepository.GetAllAsync();
        }

        /// <summary>
        /// Получить данные сотрудника по id
        /// </summary>
        /// <param name="id">Id сотрудника, например <example>9686a3684eeb8bfacc69b644</example></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<Employee> GetEmployeeById(string id)
        {
            var employee = await _employeesRepository.GetByIdAsync(id);
            if (employee is null) return null;
            return employee;
        }

        [HttpDelete("{id}")]
        public async Task Delete(string id)
        {
            var employee = await _employeesRepository.GetByIdAsync(id);
            if (employee is null)
            {
                Console.WriteLine("Employee not found");
                return;
            }

            await _employeesRepository.DeleteAsync(employee.Id);

            Ok($"Employee w/ id = {id} was delete");
        }
    }
}
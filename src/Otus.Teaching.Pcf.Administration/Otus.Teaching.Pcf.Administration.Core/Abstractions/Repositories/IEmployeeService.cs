﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;

namespace Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories
{
    public interface IEmployeeService
    {
        public Employee Create(Employee employee);
        public List<Employee> Get();
        public Employee Get(string id);
        public void Remove(string id);
        public void Update(string id, Employee employee);
    }
}
